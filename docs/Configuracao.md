# BrySdkElixir

Esta documentação é destinada a desenvolvedores que desejam integrar seu site ou aplicação com a API da Bry.

## Instalação
```elixir
def deps do
  [
    {:bry_sdk_elixir, "~> 0.1.0"}
  ]
end
```

## Configuração
Configure seus ambientes dev.exs/prod.exs. 

```elixir
config :bry_sdk_elixir,
client_id: "Client_Id",
client_secret: "Client_Secret",
debug: true
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Docs can
be found at [https://hexdocs.pm/bry_sdk_elixir/](https://hexdocs.pm/gerencianet).

