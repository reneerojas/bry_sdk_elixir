defmodule BrySdkElixir.Kms do
  @moduledoc false
  alias BrySdkElixir.Request

  def get_endpoint() do
    case Application.get_env(:bry_sdk_elixir, :env) do
      :dev -> "https://kms.dev.bry.com.br/kms/rest/v1"
      :homologacao -> "https://kms.hom.bry.com.br/kms/rest/v1"
      _ -> "https://kms.bry.com.br/kms/rest/v1"
    end
  end

  def chaves() do
     Request.get("#{get_endpoint()}/chaves")
  end

  def compartimentos() do
     Request.get("#{get_endpoint()}/compartimentos")
  end

  def assinaturas() do
    Request.get("#{get_endpoint()}/assinaturas")
  end

end
