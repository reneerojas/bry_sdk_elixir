defmodule BrySdkElixir.CmsSigner do
  @moduledoc false
  alias BrySdkElixir.Request

  def get_endpoint() do
    case Application.get_env(:bry_sdk_elixir, :env) do
      # :dev -> "https://kms.dev.bry.com.br/kms/rest/v1"
      # :homologacao -> "https://kms.hom.bry.com.br/kms/rest/v1"
      _ -> "https://hub2.bry.com.br/fw/v1"
    end
  end

  def assinaturas(kms_type, data) do
    Request.post("#{get_endpoint()}/cms/kms/assinaturas", data, [{"kms_type",kms_type}])
  end

  def verify_cms(data) do
    Request.post("https://fw2.bry.com.br/api/cms-verification-service/v1/signatures/verify", data, [{"Content-Type","multipart/form-data"}])
  end

  def verify_pdf(data) do
    Request.post("#{get_endpoint()}/pdf-verification-service/v1/signatures/verify", data, [{"Content-Type","multipart/form-data"}])
  end

end
