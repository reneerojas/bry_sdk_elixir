defmodule BrySdkElixir.MixProject do
  use Mix.Project

  def project do
    [
      app: :bry_sdk_elixir,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      docs: docs(),
      name: "BrySdk",
      source_url: "https://gitlab.com/reneerojas/bry-sdk-elixir"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.25.5"},
      {:jason, "~> 1.1"},
      {:httpoison, "~> 1.8"}
    ]
  end

  defp description() do
    "SDK de integração Bry"
  end

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "bry",
      # These are the default files included in the package
      files: ~w(lib mix.exs .formatter.exs README*),
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/reneerojas/bry-sdk-elixir"}
    ]
  end

  defp docs() do
    [
      main: "configuracao",
      formatter_opts: [gfm: true],
      source_ref: @version,
      source_url: "https://gitlab.com/reneerojas/bry-sdk-elixir",
      extras: [
        "docs/Configuracao.md"]
    ]
  end
end
